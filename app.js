const http = require('http');

// Define port to listen on
const PORT = process.env.PORT || 3000;

// Create HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello, World!\n');
});

// Start server
server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
